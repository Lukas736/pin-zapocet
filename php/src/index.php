<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous" />
    <title>Repozitář studentů</title>
    <style>
        @import url('https://fonts.googleapis.com/css2?family=Frank+Ruhl+Libre&display=swap');

        body {
            font-family: 'Frank Ruhl Libre', serif;
        }

        button {
            min-height: 30px;
            padding: 5px;
            width: fit-content;
        }

        h3 {
            height: 40px;
            color: #fff;
            background-color: #343a40;
            width: 15rem;
            padding-bottom: 5px;
            border-radius: 5px;
            font-style: italic;
        }

        h1 {
            color: #fff;
            text-align: center;
            height: 60px;
            border-bottom: solid 0.1px whitesmoke
        }
    </style>
</head>

<body class="bg-secondary bg-gradient">
    <header class="bg-dark">
        <h1>Vstupní brána do Repozitáře studentů</h1>
        <nav class="navbar navbar-dark navbar-text d-flex justify-content-center">
            <button class="p-2 btn btn-secondary" id="Formulář" style="font-size:1.2rem" onclick="goTo('templates/form.php', this.getAttribute('id'));">Formulář pro nahrání studenta</button>
            <button class="p-2 ml-2 mr-2 btn btn-secondary" id="Repozitář" style="font-size:1.2rem" onclick="goTo('public/studenti.html', this.getAttribute('id'));">Repozitář studentů</button>
            <button class="p-2 btn btn-secondary" id="XPath" style="font-size:1.2rem" onclick="goTo('templates/xpath.php', this.getAttribute('id'));">XPath</button>
            <button class="p-2 ml-2 btn btn-secondary" id="Obrázek" style="font-size:1.2rem" onclick="goTo('public/obrazek.html', this.getAttribute('id'));">Obrázek</button>
        </nav>
    </header>

    <?php
    if (isset($_COOKIE['lastPage'])) {
        $lastPage = $_COOKIE['lastPage'];
        echo "Vaše poslední stránka byla: {$lastPage}";
    }
    ?>

    <script>
        function goTo(path, id) {
            window.location.href = path
            document.cookie = "lastPage=" + id
        }
    </script>

</body>

</html>

<?php

?>