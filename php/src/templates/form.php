<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous" />
    <title>Formulář pro přidávání studentů</title>
    <style>
        @import url('https://fonts.googleapis.com/css2?family=Frank+Ruhl+Libre&display=swap');

        body {
            font-family: 'Frank Ruhl Libre', serif;
        }

        button {
            min-height: 30px;
            padding: 5px;
            width: fit-content;
        }

        h3 {
            height: 40px;
            color: #fff;
            background-color: #343a40;
            width: 15rem;
            padding-bottom: 5px;
            border-radius: 5px;
            font-style: italic;
        }

        h1 {
            color: #fff;
            text-align: center;
            height: 60px;
            border-bottom: solid 0.1px whitesmoke
        }

        section label {
            padding-left: 10px;
        }
    </style>
</head>

<body class="bg-secondary bg-gradient">
    <header style="height: fit-content;">
        <h1>Formulář pro přidání studentů</h1>
        <nav class="navbar navbar-dark">
            <a class="navbar-brand" href="/">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" width="20px" height="20px">
                    <path style="fill:white" d="M9.4 233.4c-12.5 12.5-12.5 32.8 0 45.3l160 160c12.5 12.5 32.8 12.5 45.3 0s12.5-32.8 0-45.3L109.2 288 416 288c17.7 0 32-14.3 32-32s-14.3-32-32-32l-306.7 0L214.6 118.6c12.5-12.5 12.5-32.8 0-45.3s-32.8-12.5-45.3 0l-160 160z" />
                </svg>
                Zpět
            </a>
        </nav>
    </header>
    <main class="container" style="max-width:900px; padding-bottom:50px;">
        <form action="../modules/actions/addStudent.php" method="get">
            <fieldset>
                <legend>Osobní informace</legend>
                <section style="display:grid; grid-template-rows: 0.1fr 0.1fr 0.1fr 0.1fr 0.1fr; grid-template-columns: 1fr 1fr; grid-gap:10px">
                    <label style="grid-row:1; grid-column:1;" for="jmeno">Jméno:</label>
                    <input style="grid-row:2; grid-column:1;" class="form-control" style="max-width:250px;" type="text" name="jmeno" id="_jmeno" placeholder="Jméno" required="true" />
                    <label style="grid-row:1; grid-column:2;" for="prijmeni">Příjmení:</label>
                    <input style="grid-row:2; grid-column:2;" class="form-control" style="max-width:250px;" type="text" name="prijmeni" id="_prijmeni" placeholder="Příjmení" required="true" />
                    <ul class="" style="margin:0; padding:10px;">
                        <li class="form-check" style="display:inline">
                            <input class="form-check-input" type="radio" id="_muz" name="pohlavi" value="Muž" checked />
                            <label class="form-check-label" style="padding:0;" for="_muz">Muž</label>
                        </li>
                        <li class="form-check" style="display:inline; padding-left:30px">
                            <input class="form-check-input" type="radio" id="_zena" name="pohlavi" value="Žena" />
                            <label class="form-check-label" style="padding:0;" for=" _zena">Žena</label>
                        </li>
                    </ul>
                    <label style="grid-row:4; grid-column:1" for="email">Email:</label>
                    <input style="grid-row:5; grid-column:1" class="form-control" type="email" name="email" id="_email" placeholder="E-mail" required="true" />
                    <select style="grid-row:5; grid-column:2; max-width:70px;" name="predvolba" id="_predvolba">
                        <option value="420" selected>+420</option>
                        <option value="421">+421</option>
                    </select>
                    <label style="grid-row:4; grid-column:2;" for=" telefon">Telefonní číslo:</label>
                    <input style="grid-row:5; grid-column:2; margin-left:75px; max-width:355px" class="form-control" type="tel" name="telefon" id="_telefon" placeholder="Telefonní číslo" required="false" pattern="[0-9]{3}[0-9]{3}[0-9]{3}" />
                </section>
                <section style="display:grid; grid-template-rows: 0.1fr 0.1fr 0.1fr 0.1fr 0.1fr; grid-template-columns: 1fr 1fr; padding-top:10px; grid-gap:10px;">
                    <header>
                        <h4 style="font-size:18px; grid-row: 1; grid-column:1;">Adresa bydliště:</h4>
                    </header>
                    <label style="grid-row:2; grid-column:1;" for="mesto">Město:</label>
                    <input class="form-control" style="grid-row:3; grid-column:1;" type="text" name="mesto" id="_mesto" placeholder="Město" required="true" />
                    <label style="grid-row:2; grid-column: 2;" for="psc">PSČ:</label>
                    <input class="form-control" style="grid-row:3; grid-column:2;" type="text" name="psc" id="_psc" placeholder="PSČ" required="true" pattern="+[0-9]{3} [0-9]{2}" />
                    <label style="grid-row:4; grid-column:1;" for="ulice">Ulice:</label>
                    <input class="form-control" style="grid-row:5; grid-column:1;" type="text" name="ulice" id="_ulice" placeholder="Ulice" required="true" />
                    <label style="grid-row:4; grid-column:2;" for="cisloPopisne">Číslo popisné:</label>
                    <input class="form-control" style="grid-row:5; grid-column:2;" type="text" name="cisloPopisne" id="_cisloPopisne" placeholder="Číslo popisné" required="true" />
                </section>
            </fieldset>
            <fieldset>
                <legend style="padding-top:10px">Školní informace</legend>
                <section style="display:grid; grid-template-rows: 0.1fr 0.1fr; grid-template-columns: 1fr 1fr; grid-gap:10px">
                    <label style="grid-row:1; grid-column:1;" for="cisloStudenta">Číslo studenta (ST):</label>
                    <input class="form-control" style="grid-row:2; grid-column:1;" type="text" name="cisloStudenta" id="_cisloStudenta" placeholder="Číslo studenta" required="true" />
                    <label style="grid-row:1; grid-column:2;" for="dobaStudia">Doba studia (ve dnech):</label>
                    <input class="form-control" style="grid-row:2; grid-column:2;" type="text" name="dobaStudia" id="_dobaStudia" placeholder="Doba studia" required="true" />
                </section>
                <section style="display:grid; grid-template-rows: 0.1fr 0.1fr 0.1fr 0.1fr ; grid-template-columns: 1fr 1fr; grid-gap:10px">
                    <label style="grid-row: 1; grid-column:1; padding-top:10px;" for="fakulta">Fakulta:</label>
                    <select style="grid-row: 2; grid-column:1;" name=" fakulta" id="_fakulta">
                        <option value="PŘF" selected>Přírodovědecká</option>
                        <option value="PF">Pedagogická</option>
                        <option value="FSI">Filozofická</option>
                    </select>
                    <label style="grid-row: 1; grid-column:2; padding-top:10px;" for="studijniProgram">Studijní program:</label>
                    <input style="grid-row: 2; grid-column:2;" type="text" class="form-control" name="studijniProgram" id="_studijniProgram" placeholder="Studijní program" required="true" />
                    <label style="grid-row: 3; grid-column:1;" for="fakulta">Katedra:</label>
                    <select style="grid-row: 4; grid-column:1;" name="katedra" id="_katedra">
                        <option value="KI" selected>Informatiky</option>
                        <option value="KMA">Matematiky</option>
                        <option value="KFY">Fyziky</option>
                        <option value="USE">Strojního inženýrství</option>
                    </select>
                    <label style="grid-row: 3; grid-column:2;" for="obor">Obor:</label>
                    <input style="grid-row: 4; grid-column:2;" type="text" class="form-control" name="obor" id="_obor" placeholder="Obor" required="true" />
                </section>
            </fieldset>
            <input class="btn btn-secondary" style="margin-top:10px; border: 1px solid #343a40" type="submit" value="Vytvořit studenta" />
        </form>
    </main>

    <script>
        function goTo(path) {
            window.location.href = path
        }
    </script>
</body>