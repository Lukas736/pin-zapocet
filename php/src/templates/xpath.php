<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous" />
    <title>Formulář pro přidávání studentů</title>
    <style>
        @import url('https://fonts.googleapis.com/css2?family=Frank+Ruhl+Libre&display=swap');

        body {
            font-family: 'Frank Ruhl Libre', serif;
        }

        h1 {
            color: #fff;
            text-align: center;
            height: 60px;
            border-bottom: solid 0.1px whitesmoke
        }
    </style>
</head>

<body class="bg-secondary bg-gradient">
    <header style="height: fit-content;">
        <h1>Použití XPath pro procházení XML</h1>
        <nav class="navbar navbar-dark">
            <a class="navbar-brand" href="/">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" width="20px" height="20px">
                    <path style="fill:white" d="M9.4 233.4c-12.5 12.5-12.5 32.8 0 45.3l160 160c12.5 12.5 32.8 12.5 45.3 0s12.5-32.8 0-45.3L109.2 288 416 288c17.7 0 32-14.3 32-32s-14.3-32-32-32l-306.7 0L214.6 118.6c12.5-12.5 12.5-32.8 0-45.3s-32.8-12.5-45.3 0l-160 160z" />
                </svg>
                Zpět
            </a>
        </nav>
    </header>
    <main>
        <form style="margin-left:10px; display:flex-inline;" method="get">
            <label style="font-size:18px">Zadejte XPath:</label>
            <input style="margin-left:5px; padding-left:5px;" type="text" name="path" placeholder="/studenti/student" required="true" />
            <input class="btn btn-secondary" style="margin-left:5px; border: 1px solid #343a40" type="submit" value="Projdi XML!" />
        </form>
        <?php
        if (isset($_SESSION['lastPaths'])) {
            $paths = $_SESSION['lastPaths'];
        } else {
            $paths = array();
        }
        if (count($paths) > 0) {
            echo "<ul style='list-style:none;'>";
            echo 'Poslední vyhledávané výrazy: ';
            foreach ($paths as $path) {
                echo "<li style='margin-left:10px; '>{$path}</li>";
            }
            echo '</ul>';
        }
        ?>
    </main>
</body>

<?php

if (isset($_GET['path'])) {
    $paths[] = $_GET['path'];

    if (count($paths) > 3) {
        array_shift($paths);
    }

    $_SESSION['lastPaths'] = $paths;

    $doc = new DOMDocument;
    $doc->preserveWhiteSpace = false;
    $doc->load('../studenti.xml');

    $xpath = new DOMXPath($doc);

    $result = $xpath->query(end($paths));
    echo '<ul>';
    foreach ($result as $res) {
        echo "<li>{$res->nodeValue}</li>";
    }
}
echo '</ul>';

?>