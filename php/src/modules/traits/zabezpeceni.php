<?php

namespace zabezpeceni;

trait Zabezpeceni
{

    public function predzpracuj_vstup($data)
    {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }
}
