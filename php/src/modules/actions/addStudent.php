<?php

require "../models/student.php";

use student\Student;

$jmeno = $_GET['jmeno'];
$prijmeni = $_GET['prijmeni'];
$pohlavi = $_GET['pohlavi'];
$email = $_GET['email'];
$predvolba = $_GET['predvolba'];
$telefon = $_GET['telefon'];
$mesto = $_GET['mesto'];
$psc = $_GET['psc'];
$ulice = $_GET['ulice'];
$cisloPopisne = $_GET['cisloPopisne'];
$fakulta = $_GET['fakulta'];
$studijniProgram = $_GET['studijniProgram'];
$katedra = $_GET['katedra'];
$obor = $_GET['obor'];
$cisloStudenta = $_GET['cisloStudenta'];
$dobaStudia = $_GET['dobaStudia'];

$novyStudent = new Student($jmeno, $prijmeni, $pohlavi, $email, $predvolba, $telefon, $mesto, $psc, $ulice, $cisloPopisne, $fakulta, $studijniProgram, $katedra, $obor, $cisloStudenta, $dobaStudia);

$noveXml = $novyStudent->zapisDoXML();
$noveXml->encoding = "utf-8";

// Validace pomocí DTD
$koren = 'studenti';
$generatorDokumentu = new DOMImplementation;
$doctype = $generatorDokumentu->createDocumentType($koren, "", '../../studenti.dtd');
$dokumentProValidaci = $generatorDokumentu->createDocument(null, "", $doctype);
$dokumentProValidaci->encoding = "utf-8";

$uzelStudenti = $noveXml->getElementsByTagName($koren)->item(0);
$novy_uzel = $dokumentProValidaci->importNode($uzelStudenti, true);
$dokumentProValidaci->appendChild($novy_uzel);

$dokumentProValidaci->validate();

// Validace pomocí XSD
$noveXml->schemaValidate('../../studenti.xsd');

// Přepsání původního XML
$noveXml->save("../../studenti.xml");

// XSLT transforamce 

// XSL
$xslDokument = new DOMDocument();
$xslDokument->load('../../studenti.xsl');

$xslProcesor = new XSLTProcessor();
$xslProcesor->importStylesheet($xslDokument);
$transformovaneXml = $xslProcesor->transformToDoc($noveXml);

// Přepsání existujícího html souboru
$nazevDokumentu = "../../public/studenti" . ".html";
$transformovaneXml->save($nazevDokumentu);
header('Location: ' . $nazevDokumentu);
