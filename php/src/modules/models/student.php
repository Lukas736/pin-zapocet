<?php

namespace student;

require "../interfaces/interfaces.php";
require "../traits/zabezpeceni.php";

use zabezpeceni\Zabezpeceni;
use interfaces as I;
use SimpleXMLElement;

abstract class Clovek
{
    use Zabezpeceni;

    protected $_jmeno;
    protected $_prijmeni;
    protected $_pohlavi;

    public function __construct($jmeno, $prijmeni, $pohlavi)
    {
        $this->_jmeno = $this->predzpracuj_vstup($jmeno);
        $this->_prijmeni = $this->predzpracuj_vstup($prijmeni);
        $this->_pohlavi = $this->predzpracuj_vstup($pohlavi);
    }

    abstract public function vypis_informace();
}


final class Student extends Clovek implements I\SessionZapisovatelne, I\XMLZapisovatelne
{

    private $_email;
    private $_telefon;
    private $_predvolba;
    private $_mesto;
    private $_psc;
    private $_ulice;
    private $_cisloPopisne;
    private $_fakulta;
    private $_studijniProgram;
    private $_katedra;
    private $_obor;
    private $_cisloStudenta;
    private $_dobaStudia;


    const XML_DATABAZE_STUDENTU = "studenti.xml";


    public static $pocet_vytvorenych_studentu = 0;

    public function __construct($jmeno, $prijmeni, $pohlavi, $email, $predvolba, $telefon, $mesto, $psc, $ulice, $cisloPopisne, $fakulta, $studijniProgram, $katedra, $obor, $cisloStudenta, $dobaStudia)
    {

        #volani rodicovske metody - konstruktor rodice
        parent::__construct($jmeno, $prijmeni, $pohlavi);

        #nastaveni vlastnosti odlisnych od rodice
        $this->_email = $this->predzpracuj_vstup($email);
        $this->_predvolba = $this->predzpracuj_vstup($predvolba);
        $this->_telefon = $this->predzpracuj_vstup($telefon);
        $this->_mesto = $this->predzpracuj_vstup($mesto);
        $this->_psc = $this->predzpracuj_vstup($psc);
        $this->_ulice = $this->predzpracuj_vstup($ulice);
        $this->_cisloPopisne = $this->predzpracuj_vstup($cisloPopisne);
        $this->_fakulta = $this->predzpracuj_vstup($fakulta);
        $this->_studijniProgram = $this->predzpracuj_vstup($studijniProgram);
        $this->_katedra = $this->predzpracuj_vstup($katedra);
        $this->_obor = $this->predzpracuj_vstup($obor);
        $this->_cisloStudenta = $this->predzpracuj_vstup($cisloStudenta);
        $this->_dobaStudia = $this->predzpracuj_vstup($dobaStudia);

        $this->_id = Student::pocet_studentu() + 1;

        Student::$pocet_vytvorenych_studentu += 1;
    }

    # Gettery
    function get_jmeno()
    {
        return $this->_jmeno;
    }
    function get_prijmeni()
    {
        return $this->_prijmeni;
    }
    function get_pohlavi()
    {
        return $this->_pohlavi;
    }
    function get_email()
    {
        return $this->_email;
    }
    function get_predvolba()
    {
        return $this->_predvolba;
    }
    function get_telefon()
    {
        return $this->_telefon;
    }
    function get_ulice()
    {
        return $this->_ulice;
    }
    function get_mesto()
    {
        return $this->_mesto;
    }
    function get_psc()
    {
        return $this->_psc;
    }
    function get_cisloPopisne()
    {
        return $this->_cisloPopisne;
    }
    function get_fakulta()
    {
        return $this->_fakulta;
    }
    function get_studijniProgram()
    {
        return $this->_studijniProgram;
    }
    function get_katedra()
    {
        return $this->_katedra;
    }
    function get_obor()
    {
        return $this->_obor;
    }
    function get_cisloStudenta()
    {
        return $this->_cisloStudenta;
    }
    function get_dobaStudia()
    {
        return $this->_dobaStudia;
    }

    # Settery
    function set_jmeno($value)
    {
        return $this->_jmeno = $value;
    }
    function set_prijmeni($value)
    {
        return $this->_prijmeni = $value;
    }
    function set_pohlavi($value)
    {
        return $this->_pohlavi = $value;
    }
    function set_email($value)
    {
        return $this->_email = $value;
    }
    function set_ulice($value)
    {
        return $this->_ulice = $value;
    }
    function set_mesto($value)
    {
        return $this->_mesto = $value;
    }
    function set_psc($value)
    {
        return $this->_psc = $value;
    }
    function set_cisloPopisne($value)
    {
        return $this->_cisloPopisne = $value;
    }
    function set_predvolba($value)
    {
        return $this->_predvolba = $value;
    }
    function set_telefon($value)
    {
        return $this->_telefon = $value;
    }
    function set_fakulta($value)
    {
        return $this->_fakulta = $value;
    }
    function set_studijniProgram($value)
    {
        return $this->_studijniProgram = $value;
    }
    function set_katedra($value)
    {
        return $this->_katedra = $value;
    }
    function set_obor($value)
    {
        return $this->_obor = $value;
    }
    function set_cisloStudenta($value)
    {
        return $this->_cisloStudenta = $value;
    }
    function set_dobaStudia($value)
    {
        return $this->_dobaStudia = $value;
    }

    public function vypis_informace()
    {
        echo $this->get_jmeno();
        echo $this->get_prijmeni();
        echo $this->get_pohlavi();
        echo $this->get_email();
        echo $this->get_telefon();
        echo $this->get_mesto();
        echo $this->get_psc();
        echo $this->get_ulice();
        echo $this->get_cisloPopisne();
        echo $this->get_fakulta();
        echo $this->get_studijniProgram();
        echo $this->get_katedra();
        echo $this->get_obor();
        echo $this->get_cisloStudenta();
        echo $this->get_dobaStudia();
    }

    public function zapisDoSession($klic, $hodnota)
    {
        echo "klic:" . $klic . " hodnota:" . $hodnota . " sess:" . var_dump($_SESSION[$klic]);
        if (empty($_SESSION[$klic])) {
            $_SESSION[$klic] = array($hodnota);
            echo "EMPTY! klic:" . $klic . " hodnota:" . $hodnota . " sess:" . var_dump($_SESSION[$klic]);
        } else {
            array_push($_SESSION[$klic], $hodnota);
            echo $_SESSION[$klic];
            echo "EXISTS! klic:" . $klic . " hodnota:" . $hodnota . " sess:" . var_dump($_SESSION[$klic]);
        }
    }

    public function zapisDoXML()
    {

        #zapis pomoci simpleXML do xml souboru
        $xml = simplexml_load_file('../../' . Student::XML_DATABAZE_STUDENTU) or die("Chyba: nelze nacist xml soubor " . Student::XML_DATABAZE_STUDENTU);

        $novy_student = $xml->addChild("student");

        $nova_osobniInformace = $novy_student->addChild("osobniInformace");
        $nova_osobniInformace->addChild("jmeno", $this->get_jmeno());
        $nova_osobniInformace->addChild("prijmeni", $this->get_prijmeni());
        $pohlavi = $nova_osobniInformace->addChild("pohlavi");
        $pohlavi->addAttribute("typ", $this->get_pohlavi());
        $nova_osobniInformace->addChild("email", $this->get_email());
        $telefon = $nova_osobniInformace->addChild("telefon", $this->get_telefon());
        $telefon->addAttribute("predvolba", $this->get_predvolba());

        $nova_adresaBydliste = $nova_osobniInformace->addChild("adresaBydliste");
        $nova_adresaBydliste->addChild("mesto", $this->get_mesto());
        $nova_adresaBydliste->addChild("psc", $this->get_psc());
        $nova_adresaBydliste->addChild("ulice", $this->get_ulice());
        $nova_adresaBydliste->addChild("cisloPopisne", $this->get_cisloPopisne());

        $nova_skolniInformace = $novy_student->addChild("skolniInformace");
        $studijni_program = $nova_skolniInformace->addChild("studijniProgram", $this->get_studijniProgram());
        $studijni_program->addAttribute("fakulta", $this->get_fakulta());
        $obor = $nova_skolniInformace->addChild("obor", $this->get_obor());
        $obor->addAttribute("katedra", $this->get_katedra());
        $nova_skolniInformace->addChild("cisloStudenta", $this->get_cisloStudenta());
        $nova_skolniInformace->addChild("dobaStudia", $this->get_dobaStudia());

        return dom_import_simplexml($xml)->ownerDocument;
    }

    public static function pocet_studentu()
    {
        #simpleXml - nacte xml dokument
        $xml = simplexml_load_file('../../' . Student::XML_DATABAZE_STUDENTU) or die("Chyba: nelze nacist xml soubor studenti.xml");

        #vrat pocet potomku rootu
        return $xml->count();
    }
}
