<?php
# Definice jmenného prostoru
namespace interfaces;

interface XMLZapisovatelne
{
    public function zapisDoXML();
}

interface SessionZapisovatelne
{
    public function zapisDoSession($klic, $hodnota);
}
