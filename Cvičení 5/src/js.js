const buttons = document.getElementsByTagName("button")

for (let button of buttons) {
    button.addEventListener("click", showInfo(button.nextSibling))
}

function showInfo(button) {
  if (button.style.display === "none") {
    button.style.display = "block";
  } else {
    button.style.display = "none";
  }
} 

