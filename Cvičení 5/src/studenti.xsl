<?xml version="1.0" encoding="UTF-8"?>
<xsl:transform version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:key name="index" match="*" use="@fakulta" />
    <xsl:output method="html"/>
    <xsl:template match="/">
        <html>
            <head>
                <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous" />
                <meta name="viewport" content="width=device-width,initial-scale=1"></meta>
                <title>Student</title>
                <style>
                    button {
                        min-height:40px; 
                        padding-top:5px; 
                        width:100%; 
                        font-size:1.2rem
                    }
                    h3 {
                        height:40px; 
                        color:#fff; 
                        background-color: #343a40; 
                        width:15rem;
                        padding-bottom:5px;
                        border-radius:5px;
                        font-style:italic;
                    }
                    h2 { 
                        color:#fff; 
                        background-color: #343a40; 
                        font-size:1.3rem;
                        margin:0;
                    }
                    h1 {
                        color:#fff;
                        text-align:center;
                        height:60px;
                        border-bottom: solid 0.1px whitesmoke

                    }
                    .bi-arrow-90deg-left::before {
                        display:none
                    }


                </style>
            </head>
            <body class="bg-secondary bg-gradient">

                <header class="bg-dark">
                    <h1 >Repozitář studentů</h1>
                    <nav class="navbar navbar-dark">
                        <a class="navbar-brand" href="/">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" width="20px" height="20px"><path  style="fill:white" d="M9.4 233.4c-12.5 12.5-12.5 32.8 0 45.3l160 160c12.5 12.5 32.8 12.5 45.3 0s12.5-32.8 0-45.3L109.2 288 416 288c17.7 0 32-14.3 32-32s-14.3-32-32-32l-306.7 0L214.6 118.6c12.5-12.5 12.5-32.8 0-45.3s-32.8-12.5-45.3 0l-160 160z"/></svg>
                            Zpět
                        </a>
                        <section style="width:100px; display:flex; gap:15px;">
                            <xsl:for-each select="//*[generate-id() = generate-id(key('index',@fakulta)[1])]">
                                <h2><xsl:value-of select="@fakulta" /></h2>
                            </xsl:for-each>
                        </section>
                    </nav>
                </header>

                    
                <main style="padding-top:5vh">
                    <section style="max-width:1200px">
                        <ul style="list-style-type:none">
                            <xsl:for-each select="studenti/student">
                                <xsl:sort select="osobniInformace/jmeno" />
                                <li>
                                    <arcticle class="container-lg text-center row">
                                        <header>
                                            <h3> 
                                                <xsl:value-of select="osobniInformace/jmeno" />
                                            </h3>
                                        </header>
                                        <section class="col" style="max-width:500px">
                                            <button class="btn" style="background-color: #343a40; color:#fff;" onclick="showInfo(this)">Osobní informace</button>
                                            <xsl:apply-templates select="osobniInformace"/>
                                        </section>
                                        <section class="col" style="max-width:500px">
                                            <button class="btn" style="background-color: #343a40; color:#fff;" onclick="showInfo(this)">Školní informace</button>
                                            <xsl:apply-templates select="skolniInformace"/>
                                        </section>
                                    </arcticle>
                                </li>
                            </xsl:for-each>
                        </ul>
                    </section>
                </main>
                <script>

                    function showInfo(button) {
                        if (button.nextSibling.style.display === "none") {
                            button.nextSibling.style.display = "table";
                        } else {
                            button.nextSibling.style.display = "none";
                        }
                    } 

                </script>
            </body>
        </html>
    </xsl:template>

    <xsl:template match="osobniInformace">    
        <table class="table table-dark table-striped table-hover" style="display:none; margin-top:10px">
            <tbody class="table-group-divider table-borderless">
                <tr>
                    <td>Jméno:</td>
                    <td colspan="2"><xsl:value-of select="jmeno" /></td>
                    <td></td>
                </tr>
                <tr>
                    <td>Příjmení:</td>
                    <td colspan="2"><xsl:value-of select="prijmeni" /></td>
                    <td></td>
                </tr>
                <tr>
                    <td>Pohlaví:</td>
                    <td colspan="2"><xsl:value-of select="pohlavi/@typ" /></td>
                    <td></td>
                </tr>
                <tr>
                    <td>Email:</td>
                    <td colspan="2"><xsl:value-of select="email" /></td>
                    <td></td>
                </tr>
                <tr>
                    <td>Telefon:</td>
                    <td colspan="2"><xsl:value-of select="telefon" /></td>
                    <td></td>
                </tr>
                <tr>
                    <td style="">Adresa:</td>
                    <td><xsl:value-of select="adresaBydliste/ulice" /> </td>
                    <td colspan="2"><xsl:value-of select="adresaBydliste/cisloPopisne" /></td>
                </tr>
                <tr>
                    <td></td>
                    <td><xsl:value-of select="adresaBydliste/mesto" /> </td>
                    <td colspan="2"><xsl:value-of select="adresaBydliste/psc" /></td>
                </tr>
            </tbody>    
        </table>
     </xsl:template>

     <xsl:template match="skolniInformace">
        <table class="table table-dark table-striped table-hover" style="display:none; margin-top:10px">
            <tbody class="table-group-divider table-borderless">
                <tr>
                    <td>Studijní program:</td>
                    <td><xsl:value-of select="studijniProgram" /></td>
                </tr>
                <tr>
                    <td>Obor:</td>
                    <td><xsl:value-of select="obor" /></td>
                </tr>
                <tr>
                    <td>Číslo studenta:</td>
                    <td><xsl:value-of select="cisloStudenta" /></td>
                </tr>
                <tr>
                    <td>Doba studia:</td>
                    <td><xsl:value-of select="dobaStudia" /> dní</td>
                </tr>
            </tbody>    
        </table>
    </xsl:template>

</xsl:transform>