<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous" />
    <title>Repozitář studentů</title>
    <style>
        button {
            min-height: 30px;
            padding: 5px;
            width: fit-content;
            font-size: 0.9rem;
        }

        h3 {
            height: 40px;
            color: #fff;
            background-color: #343a40;
            width: 15rem;
            padding-bottom: 5px;
            border-radius: 5px;
            font-style: italic;
        }

        h1 {
            color: #fff;
            text-align: center;
            height: 60px;
            border-bottom: solid 0.1px whitesmoke
        }
    </style>
</head>

<body class="bg-secondary bg-gradient">
    <h1>Vstupní brána do Repozitáře studentů</h1>
    <form enctype="multipart/form-data" method="POST">
        <label for="studenti">Kliknutím nahrajte studenta ve validním XML.</label>
        <input type="file" name="studenti" data-max-file-size="2M" />
        <button type="submit">Odeslat</button>
    </form>

    <button onclick="goToRepository()">Přesun do repozitáře studentů</button>

    <script>
        function goToRepository() {
            window.location.href = "./transformovane/studenti.xml.html"
        }
    </script>
</body>

</html>

<?php
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $adresar_studentu = 'studenti/';
    $nahrany_student = $adresar_studentu . basename($_FILES['studenti']['name']);

    // if (file_exists($nahrany_student)) {
    //     echo '<p class="text-danger">Soubor se stejným názvem již existuje v databázi. Prosím přejmenujte soubor.!</p>';
    // } else if (move_uploaded_file($_FILES['studenti']['tmp_name'], $nahrany_student)) {

    // XSD validace
    $xml = new DOMDocument;
    $xml->load($nahrany_student);
    if ($xml->schemaValidate('studenti.xsd')) {

        echo '<p class="text-success">Nahraný soubor je validní a byl úspěšně nahrán do databáze.</p>';

        // XML
        $xml_dokument = new DOMDocument();
        $xml_dokument->load($nahrany_student);

        // XSL
        $xsl_dokument = new DOMDocument();
        $xsl_dokument->load("studenti.xsl");

        // XSLTtransformation
        $xsl_procesor = new XSLTProcessor();
        $xsl_procesor->importStylesheet($xsl_dokument);
        $transformovany_xml = $xsl_procesor->transformToDoc($xml_dokument);

        // ulozeni transformovaneho dokumentu
        $nazev_dokumentu = basename($_FILES['studenti']['name']) . ".html";
        $transformovany_xml->save("transformovane/" . $nazev_dokumentu);
    } else {
        echo '<p class="text-warning">Nahraný soubor není validní! Prosím zkontrolujte správnou strukturu.</p>';
        unlink($nahrany_student);
    }
} else {
    echo '<p class="text-danger">Došlo k chybě při nahrávání souboru!</p>';
    // }
}
?>