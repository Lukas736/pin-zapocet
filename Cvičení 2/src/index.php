<h1>Studentonahrávač</h1>
<form enctype="multipart/form-data" method="POST">
    <label for="dtd">Kliknutím nahrajte studenta ve validním XML souboru s ověřením <strong>DTD</strong>.</label>
    <input type="file" name="dtd" data-max-file-size="2M" />
    <button type="submit">Odeslat</button>
</form>
<form enctype="multipart/form-data" method="POST">
    <label for="xsd">Kliknutím nahrajte studenta ve validním XML souboru s ověřením <strong>XSD</strong>.</label>
    <input type="file" name="xsd" data-max-file-size="2M" />
    <button type="submit">Odeslat</button>
</form>

<?php
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $adresar_studentu = './studenti/';
    if (isset($_FILES["dtd"])) {
        $nahrany_student = $adresar_studentu . basename($_FILES['dtd']['name']);

        if (file_exists($nahrany_student)) {
            echo '<p class="text-danger">Soubor se stejným názvem již existuje v databázi. Prosím přejmenujte soubor.!</p>';
        } else if (move_uploaded_file($_FILES['dtd']['tmp_name'], $nahrany_student)) {
            $puvodni_xml = new DOMDocument();
            $puvodni_xml->load($nahrany_student);
            $koren = 'student';
            $generator_dokumentu = new DOMImplementation;
            $doctype = $generator_dokumentu->createDocumentType($koren, "", 'student.dtd');
            $novy_xml = $generator_dokumentu->createDocument(null, "", $doctype);
            $novy_xml->encoding = "utf-8";

            $puvodni_uzel = $puvodni_xml->getElementsByTagName($koren)->item(0);
            $novy_uzel = $novy_xml->importNode($puvodni_uzel, true);
            $novy_xml->appendChild($novy_uzel);

            if ($novy_xml->validate()) {
                echo '<p>Nahraný soubor je validní a byl úspěšně nahrán do databáze.</p>';
            } else {
                echo '<p>Nahraný soubor není validní! Prosím zkontrolujte správnou strukturu.</p>';
                unlink($nahrany_student);
            }
        } else {
            echo '<p>Došlo k chybě při nahrávání souboru!</p>';
        }
    }
    if (isset($_FILES["xsd"])) {
        $nahrany_student = $adresar_studentu . basename($_FILES['xsd']['name']);

        if (file_exists($nahrany_student)) {
            echo '<p class="text-danger">Soubor se stejným názvem již existuje v databázi. Prosím přejmenujte soubor.!</p>';
        } else if (move_uploaded_file($_FILES['xsd']['tmp_name'], $nahrany_student)) {

            // XSD validace
            $xml = new DOMDocument;
            $xml->load($nahrany_student);
            if ($xml->schemaValidate('student.xsd')) {

                echo '<p class="text-success">Nahraný soubor je validní a byl úspěšně nahrán do databáze.</p>';
            } else {
                echo '<p class="text-warning">Nahraný soubor není validní! Prosím zkontrolujte správnou strukturu.</p>';
                unlink($nahrany_student);
            }
        } else {
            echo '<p class="text-danger">Došlo k chybě při nahrávání souboru!</p>';
        }
    }
}
?>