<?xml version="1.0" encoding="UTF-8"?>
<xsl:transform version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html"/>
    <xsl:template match="/">
        <html>
            <head>
                <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous" />
                <meta name="viewport" content="width=device-width,initial-scale=1"></meta>
                <title>Student</title>
                <style>
                    button {
                        min-height:40px; 
                        padding-top:5px; 
                        width:100%; 
                        font-size:1.2rem
                    }
                    h3 {
                        height:40px; 
                        color:#fff; 
                        background-color: #343a40; 
                        width:15rem;
                        padding-bottom:5px;
                        border-radius:5px;
                    }
                    h1 {
                        color:#fff;
                    }
                </style>
            </head>
            <body class="bg-secondary bg-gradient">

                <header class="">
                    <h1 style="text-align:center">Repozitář studentů</h1>
                </header>

                <nav class="">
                    <a></a>
                </nav>

                <main style="padding-top:5vh">
                    <section style="max-width:1200px">
                        <ul style="list-style-type:none">
                            <xsl:for-each select="student">
                                <xsl:sort select="osobniInformace/jmeno" />
                                <li>
                                    <arcticle class="container-lg text-center row">
                                        <xsl:attribute name="id">
                                            <xsl:value-of select="concat('article', position())" />
                                        </xsl:attribute>
                                        <header>
                                            <h3> 
                                                <xsl:value-of select="osobniInformace/jmeno" />
                                            </h3>
                                        </header>
                                        <section class="col" style="max-width:500px">
                                            <button class="btn" style="background-color: #343a40; color:#fff;" onclick="showInfo(this)">Osobní informace</button>
                                            <xsl:apply-templates select="osobniInformace"/>
                                        </section>
                                        <section class="col" style="max-width:500px">
                                            <button class="btn" style="background-color: #343a40; color:#fff;" onclick="showInfo(this)">Školní informace</button>
                                            <xsl:apply-templates select="skolniInformace"/>
                                        </section>
                                    </arcticle>
                                </li>
                            </xsl:for-each>
                        </ul>
                    </section>
                </main>
                <script>

                    function showInfo(button) {
                        if (button.nextSibling.style.display === "none") {
                            button.nextSibling.style.display = "table";
                        } else {
                            button.nextSibling.style.display = "none";
                        }
                    } 

                </script>
            </body>
        </html>
    </xsl:template>

    <xsl:template match="osobniInformace">    
        <table class="table table-dark table-striped table-hover" style="display:none; margin-top:10px">
            <tbody class="table-group-divider table-borderless">
                <tr>
                    <td>Jméno:</td>
                    <td><xsl:value-of select="jmeno" /></td>
                </tr>
                <tr>
                    <td>Příjmení:</td>
                    <td><xsl:value-of select="prijmeni" /></td>
                </tr>
                <tr>
                    <td>Pohlaví:</td>
                    <td><xsl:value-of select="pohlavi/@typ" /></td>
                </tr>
                <tr>
                    <td>Email:</td>
                    <td><xsl:value-of select="email" /></td>
                </tr>
                <tr>
                    <td>Telefon:</td>
                    <td><xsl:value-of select="telefon" /></td>
                </tr>
                <tr>
                    <td style="text-align:center; padding-right:40px" colspan="2">Adresa:</td>
                </tr>
                </tr>
                <tr>
                    <td><xsl:value-of select="adresaBydliste/ulice" /> </td>
                    <td> <xsl:value-of select="adresaBydliste/cisloPopisne" /></td>
                </tr>
                <tr>
                    <td><xsl:value-of select="adresaBydliste/mesto" /> </td>
                    <td><xsl:value-of select="adresaBydliste/psc" /></td>
            </tbody>    
        </table>
     </xsl:template>

     <xsl:template match="skolniInformace">
        <table class="table table-dark table-striped table-hover" style="display:none; margin-top:10px">
            <tbody class="table-group-divider table-borderless">
                <tr>
                    <td>Studijní program:</td>
                    <td><xsl:value-of select="studijniProgram" /></td>
                </tr>
                <tr>
                    <td>Obor:</td>
                    <td><xsl:value-of select="obor" /></td>
                </tr>
                <tr>
                    <td>Číslo studenta:</td>
                    <td><xsl:value-of select="cisloStudenta" /></td>
                </tr>
                <tr>
                    <td>Doba studia:</td>
                    <td><xsl:value-of select="dobaStudia" /> dní</td>
                </tr>
            </tbody>    
        </table>
    </xsl:template>

</xsl:transform>